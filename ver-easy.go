package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/Fruzenshtein/semver-easy/packages/commits"
	"gitlab.com/Fruzenshtein/semver-easy/packages/parsers"
	"gitlab.com/Fruzenshtein/semver-easy/packages/tags"
	"gitlab.com/Fruzenshtein/semver-easy/packages/versions"
)

func main() {

	projectId := flag.String("projectId", "", "set a project ID which you want to work with")
	token := flag.String("token", "", "private token to perform API calls")
	flag.Parse()
	if *projectId == "" {
		flag.PrintDefaults()
		os.Exit(1)
	}
	if *token == "" {
		flag.PrintDefaults()
		os.Exit(1)
	}

	fmt.Printf("projectId: %v\n", projectId)
	fmt.Printf("token: %v\n", token)

	tagsResp := tags.GetTags(*projectId)
	tagsJson := parsers.ParseResponse(tagsResp.Body)
	fmt.Println((tagsJson[0]).(map[string]interface{})["name"])
	tagVersion := (tagsJson[0]).(map[string]interface{})["name"]

	commitsRest := commits.GetCommits(*projectId)
	commitsJson := parsers.ParseResponse(commitsRest.Body)
	lastCommitMsg := fmt.Sprintf("%v", (commitsJson[0]).(map[string]interface{})["title"])

	fmt.Printf("Last commit message: %v\n", lastCommitMsg)

	incrementV := parsers.DefineIncrement(lastCommitMsg)
	fmt.Printf("Increment version: %v\n", incrementV)

	version, err := parsers.ParseVerson(tagVersion.(string))
	if err != nil {
		fmt.Errorf(err.Error())
	}

	fmt.Printf("Previous version: v%v.%v.%v\n", version.Major, version.Minor, version.Patch)
	newVersion := versions.IncrementVersion(version, incrementV)

	tags.PostTags(*projectId, *token, newVersion)

	os.Setenv("NEW_VERSION", fmt.Sprintf("%v.%v.%v", newVersion.Major, newVersion.Minor, newVersion.Patch))
}
