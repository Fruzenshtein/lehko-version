## Commit requirements

Every commit should be formatted in the following way:
1. Start with one of the keywords depending on what was done: *wip*, *fix*, *refactor*, *feature* or *release*
2. Optional: then you can add *!* in case if the change should bump a major version of the release 
3. Then add *:*
4. Write a title for the commit. Entire length (including previous steps) must be less than 45 characters
5. Optional: press enter and write a description. It can be multiline.

Example commit message:
```
feature!: bump version based on commit title

based on a prefix of the commit title, bump a version of the release:
- *release* and *feature* increment a MAJOR part of the version
- *fix* increments a MINOR part of the version
- *refactor* increments a PATCH part of the version
- *wip* has no affect on any part of the version
- ! appended to the prefixes *fix*, *feature* or *release* increments a MAJOR part of the version
```

Here is a regex that validates a commit message: 

```
(wip|fix|refactor|feature|release)!?:[\w "'.,-]{5,45}\n*[\w "'.!#@*&(),:\-\n]+
```

## Build instructions

There are a few options to an executable of **lehko-version**. Firstly pull this repo code and then navigate in the terminal to its root directory.

### Option #1
Create a standard executable

```
go build
```

### Option #2
Create a minimized executable

```
go build -ldflags="-s -w"

upx --brute lehko-version
```