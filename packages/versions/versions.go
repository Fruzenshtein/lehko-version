package versions

import . "gitlab.com/Fruzenshtein/semver-easy/packages/parsers"

func IncrementVersion(current Version, incr Version) Version {
	if incr.Major == 1 {
		return Version{current.Major + 1, 0, 0}
	}
	if incr.Minor == 1 {
		return Version{current.Major, current.Minor + 1, 0}
	}
	if incr.Patch == 1 {
		return Version{current.Major, current.Minor, current.Patch + 1}
	}
	return current
}
