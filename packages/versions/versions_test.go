package versions

import (
	"testing"

	. "gitlab.com/Fruzenshtein/semver-easy/packages/parsers"
)

func TestIncrementVersion(t *testing.T) {
	parameters := []struct {
		inputCurrentV Version
		inputIncrV    Version
		expected      Version
	}{
		{Version{1, 2, 5}, Version{0, 0, 0}, Version{1, 2, 5}},
		{Version{1, 2, 5}, Version{1, 0, 0}, Version{2, 0, 0}},
		{Version{1, 2, 5}, Version{0, 1, 0}, Version{1, 3, 0}},
		{Version{1, 2, 5}, Version{0, 0, 1}, Version{1, 2, 6}},
	}

	for i := range parameters {
		actual := IncrementVersion(parameters[i].inputCurrentV, parameters[i].inputIncrV)
		if actual != parameters[i].expected {
			t.Errorf("IncrementVersion(\"%v\", \"%v\") failed, expected: %v, actual: %v", parameters[i].inputCurrentV, parameters[i].inputIncrV, parameters[i].expected, actual)
		}
	}
}
