package parsers

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	s "strings"
)

type JsonArray []interface{}
type Version struct {
	Major int
	Minor int
	Patch int
}

func ParseResponse(body io.ReadCloser) JsonArray {
	bodyStream, err := ioutil.ReadAll(body)
	if err != nil {
		log.Fatalln(err)
	}
	var result JsonArray
	json.Unmarshal(bodyStream, &result)
	parsedJson := result
	return parsedJson
}

func ParseVerson(tagVersion string) (Version, error) {
	versionText := ""
	if s.HasPrefix(tagVersion, "v") {
		versionText = tagVersion[1:]
	} else {
		versionText = tagVersion
	}
	versionParts := s.Split(versionText, ".")

	if len(versionParts) != 3 {
		return Version{}, fmt.Errorf("tag version format must be vX.Y.Z or X.Y.Z — %v", tagVersion)
	}

	majorV, err := ParsePositiveInt(versionParts[0])
	if err != nil {
		return Version{}, errors.New(err.Error())
	}
	minorV, err := ParsePositiveInt(versionParts[1])
	if err != nil {
		return Version{}, errors.New(err.Error())
	}
	patchV, err := ParsePositiveInt(versionParts[2])
	if err != nil {
		return Version{}, errors.New(err.Error())
	}

	return Version{Major: majorV, Minor: minorV, Patch: patchV}, nil
}

func ParsePositiveInt(maybeV string) (int, error) {
	intV, err := strconv.ParseInt(maybeV, 10, 64)
	if err != nil {
		return -1, fmt.Errorf("version must be int: %v", maybeV)
	}
	if intV < 0 {
		return -1, fmt.Errorf("version must be positive int: %v", maybeV)
	}

	return int(intV), nil
}

func DefineIncrement(commitMsg string) Version {
	validMatch, _ := regexp.MatchString("^(wip|fix|refactor|feature|release)!?:", commitMsg)
	if !validMatch {
		log.Fatalf("Unacceptable commmit message: %v", commitMsg)
	}

	matchForceMajorV, _ := regexp.MatchString("^((fix|feature|release)!)", commitMsg)
	if matchForceMajorV {
		return Version{1, 0, 0}
	}
	matchMajorV, _ := regexp.MatchString("^((feature|release))", commitMsg)
	if matchMajorV {
		return Version{1, 0, 0}
	}
	matchMinorV, _ := regexp.MatchString("^((fix))", commitMsg)
	if matchMinorV {
		return Version{0, 1, 0}
	}
	matchPatchV, _ := regexp.MatchString("^((refactor))", commitMsg)
	if matchPatchV {
		return Version{0, 0, 1}
	}
	return Version{0, 0, 0}
}
