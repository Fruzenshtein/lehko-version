package parsers

import "testing"

func TestParseNonIntVersion(t *testing.T) {
	errorMsg := "version must be int: z"
	_, err := ParsePositiveInt("z")
	if err.Error() != errorMsg {
		t.Errorf("ParsePositiveInt(\"z\") failed, expected: %v, actual: %v", errorMsg, err.Error())
	}
}

func TestParseNegativeIntVersion(t *testing.T) {
	errorMsg := "version must be positive int: -1"
	_, err := ParsePositiveInt("-1")
	if err.Error() != errorMsg {
		t.Errorf("ParsePositiveInt(\"-1\") failed, expected: %v, actual: %v", errorMsg, err.Error())
	}
}

func TestParsePositiveIntVersion(t *testing.T) {
	parameters := []struct {
		input    string
		expected int
	}{
		{"0", 0}, {"1", 1}, {"100", 100},
	}

	for i := range parameters {
		actual, err := ParsePositiveInt(parameters[i].input)
		if err != nil {
			t.Errorf("ParsePositiveInt(\"%v\") failed, expected: %v, actual: %v", parameters[i].input, parameters[i].expected, err.Error())
		}
		if actual != parameters[i].expected {
			t.Errorf("ParsePositiveInt(\"%v\") failed, expected: %v, actual: %v", parameters[i].input, parameters[i].expected, actual)
		}
	}
}

func TestParseVersion(t *testing.T) {
	parameters := []struct {
		input    string
		expected Version
	}{
		{"v0.0.1", Version{0, 0, 1}}, {"0.0.2", Version{0, 0, 2}},
	}

	for i := range parameters {
		actual, err := ParseVerson(parameters[i].input)
		if err != nil {
			t.Errorf("ParseVerson(\"%v\") failed, expected: %v, actual: %v", parameters[i].input, parameters[i].expected, err.Error())
		}
		if actual != parameters[i].expected {
			t.Errorf("ParseVerson(\"%v\") failed, expected: %v, actual: %v", parameters[i].input, parameters[i].expected, actual)
		}
	}

}

func TestParseIntVersionWithTooManyElements(t *testing.T) {
	errorMsg := "tag version format must be vX.Y.Z or X.Y.Z — 0.0.1.9"
	_, err := ParseVerson("0.0.1.9")
	if err.Error() != errorMsg {
		t.Errorf("ParseVerson(\"0.0.1.9\") failed, expected: %v, actual: %v", errorMsg, err.Error())
	}
}

func TestParseVersionWithNegativeParts(t *testing.T) {
	parameters := []struct {
		input    string
		expected string
	}{
		{"8.0.-1", "version must be positive int: -1"},
		{"0.-2.5", "version must be positive int: -2"},
		{"-3.2.0", "version must be positive int: -3"},
	}

	for i := range parameters {
		actual, err := ParseVerson(parameters[i].input)
		if err == nil {
			t.Errorf("ParseVerson(\"%v\") failed, expected: %v, actual: %v", parameters[i].input, parameters[i].expected, err)
		}
		if (actual != Version{}) {
			t.Errorf("ParseVerson(\"%v\") failed, expected: %v, actual: %v", parameters[i].input, parameters[i].expected, actual)
		}
	}

}

func TestDefineIncrementVersion(t *testing.T) {
	parameters := []struct {
		input    string
		expected Version
	}{
		{"fix!: now it works", Version{1, 0, 0}},
		{"feature!: cool feature", Version{1, 0, 0}},
		{"release!: time to prod", Version{1, 0, 0}},
		{"feature: cool feature", Version{1, 0, 0}},
		{"release: time to prod", Version{1, 0, 0}},
		{"fix: now it works", Version{0, 1, 0}},
		{"refactor: code structure update", Version{0, 0, 1}},
		{"wip: code structure update", Version{0, 0, 0}},
	}

	for i := range parameters {
		actual := DefineIncrement(parameters[i].input)
		if actual != parameters[i].expected {
			t.Errorf("DefineIncrement(\"%v\") failed, expected: %v, actual: %v", parameters[i].input, parameters[i].expected, actual)
		}
	}
}
