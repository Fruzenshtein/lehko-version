package commits

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

type ArrayOfObj []interface{}

func GetCommits(projectId string) *http.Response {
	url := fmt.Sprintf("https://gitlab.com/api/v4/projects/%v/repository/commits", projectId)
	resp, err := http.Get(url)
	if err != nil {
		log.Fatalln(err)
	}
	if resp.StatusCode != 200 {
		body, _ := ioutil.ReadAll(resp.Body)
		bodyString := string(body)
		log.Fatalln(fmt.Sprintf("Response: %v\nBody: %v\n", resp.StatusCode, bodyString))
	}
	/*
		body, _ := ioutil.ReadAll(resp.Body)
		bodyString := string(body)
		fmt.Println(bodyString)
	*/
	return resp
}
