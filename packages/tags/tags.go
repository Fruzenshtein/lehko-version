package tags

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	. "gitlab.com/Fruzenshtein/semver-easy/packages/parsers"
)

func GetTags(projectId string) *http.Response {
	url := fmt.Sprintf("https://gitlab.com/api/v4/projects/%v/repository/tags", projectId)
	resp, err := http.Get(url)
	if err != nil {
		log.Fatalln(err)
	}
	if resp.StatusCode != 200 {
		body, _ := ioutil.ReadAll(resp.Body)
		bodyString := string(body)
		log.Fatalln(fmt.Sprintf("Response: %v\nBody: %v\n", resp.StatusCode, bodyString))
	}
	return resp
}

func PostTags(projectId string, token string, version Version) *http.Response {
	v := fmt.Sprintf("%v.%v.%v", version.Major, version.Minor, version.Patch)
	body := make(map[string]string)
	body["tag_name"] = v
	body["ref"] = "master"
	jsonBody, err := json.Marshal(body)

	req, err := http.NewRequest("POST",
		fmt.Sprintf("https://gitlab.com/api/v4/projects/%v/repository/tags", projectId),
		bytes.NewBuffer([]byte(jsonBody)))
	req.Header.Set("PRIVATE-TOKEN", token)
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatalln(err)
	}
	if resp.StatusCode != 201 {
		bodyInput, _ := ioutil.ReadAll(resp.Body)
		bodyString := string(bodyInput)
		log.Fatalln(fmt.Sprintf("Response: %v\nBody: %v\n", resp.StatusCode, bodyString))
	}

	bodyInput, _ := ioutil.ReadAll(resp.Body)
	bodyString := string(bodyInput)
	fmt.Printf("Tag was pushed successfully: %v", bodyString)

	return resp
}
